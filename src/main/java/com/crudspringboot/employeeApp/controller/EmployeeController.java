package com.crudspringboot.employeeApp.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.crudspringboot.employeeApp.exception.ResourceNotFoundException;
import com.crudspringboot.employeeApp.model.Employee;
import com.crudspringboot.employeeApp.repository.EmployeeRepository;
import com.crudspringboot.employeeApp.serviceimpl.EmployeeServiceImpl;
//@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api/v1")
public class EmployeeController { 
	
	@Autowired
	private EmployeeRepository employeeRepository;
	
	@Autowired
	private EmployeeServiceImpl employeeService;
	
	
	
	
	@GetMapping("/employees") 
	public List<Employee> getAllEmployees() {		
		return  employeeService.getAllEmployees();
	}
	
	@GetMapping("/employee/{id}")
	public ResponseEntity<Employee> getEmpById(@PathVariable("id") Long empId) throws ResourceNotFoundException
	{
		 return employeeService.getEmpById(empId);
	}
	
	@PostMapping("/employees")
	public Employee createEmployee(@Valid @RequestBody Employee employee)
	{
		return employeeService.save(employee);
	}
	
	@PutMapping("/employees/{id}")
	
	public ResponseEntity<Employee> updateEmployee(@PathVariable("id") Long empId,@Valid @RequestBody Employee empDetails) throws ResourceNotFoundException
	{
		return employeeService.updateEmployee(empId, empDetails);
		
	}
	
	@DeleteMapping("/employees/{id}")
	public Map<String, Boolean> deleteEmployee(@PathVariable(value = "id") Long employeeId)
			throws ResourceNotFoundException {
		return employeeService.deleteEmployee(employeeId);
	}
	
	

}
