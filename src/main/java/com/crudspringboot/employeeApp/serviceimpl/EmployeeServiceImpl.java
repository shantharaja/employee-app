package com.crudspringboot.employeeApp.serviceimpl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import com.crudspringboot.employeeApp.exception.ResourceNotFoundException;
import com.crudspringboot.employeeApp.model.Employee;
import com.crudspringboot.employeeApp.repository.EmployeeRepository;
import com.crudspringboot.employeeApp.service.EmployeeService;

@Service
public class EmployeeServiceImpl implements EmployeeService {
	@Autowired
	EmployeeRepository repository;

	@Override
	public List<Employee> getAllEmployees() {

		List<Employee> pagedResult = repository.findAll();

		if (!pagedResult.isEmpty()) {
			return pagedResult;
		} else {
			return new ArrayList<Employee>();
		}
	}

	@Override
	public ResponseEntity<Employee> getEmpById(Long empId) {
		Employee employee = null;
		try {
			employee = repository.findById(empId)
					.orElseThrow(() -> new ResourceNotFoundException("Employee not found for this id :: " + empId));
		} catch (ResourceNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return ResponseEntity.ok().body(employee);
	}

	@Override
	public Employee createEmployee(Employee employee) {
		return repository.save(employee);
	}

	public ResponseEntity<Employee> updateEmployee(Long empId, Employee empDetails) {
		Employee employee=null;
		Employee updatedEmployee = null;
		try {
			employee = repository.findById(empId)
					.orElseThrow(() -> new ResourceNotFoundException("Employee not found for this id ::" + empId));
			employee.setFirstName(empDetails.getFirstName());
			employee.setLastName(empDetails.getLastName());
			employee.setEmailId(empDetails.getEmailId());
			updatedEmployee = repository.save(employee);
			
		} catch (ResourceNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return ResponseEntity.ok(updatedEmployee);

	}
	
	public Map<String, Boolean> deleteEmployee(Long employeeId){
		Employee employee;
		Map<String, Boolean> response = new HashMap<>();
		try {
			employee = repository.findById(employeeId)
					.orElseThrow(() -> new ResourceNotFoundException("Employee not found for this id :: " + employeeId));
			repository.delete(employee);
			
			response.put("deleted", Boolean.TRUE);
			
		} catch (ResourceNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return response;
	}
	

}
