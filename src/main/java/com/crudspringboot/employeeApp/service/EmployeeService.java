package com.crudspringboot.employeeApp.service;

import java.util.List;
import java.util.Map;

import org.springframework.http.ResponseEntity;

import com.crudspringboot.employeeApp.model.Employee;

public interface EmployeeService {
	public List<Employee> getAllEmployees();
	public ResponseEntity<Employee> getEmpById(Long empId);
	public Employee createEmployee(Employee employee);
	public ResponseEntity<Employee> updateEmployee(Long empId,Employee empDetails);
	public Map<String, Boolean> deleteEmployee(Long employeeId);
	
}
